#!/usr/bin/python3

"""
Use rope refactoring feature to rename sqlparse imports after bundling it
into djongo module
"""


import logging
from typing import Tuple

from rope.base.project import Project
from rope.refactor.rename import Rename
from rope.base.change import ChangeSet


LOGGER = logging.getLogger("EmbedSqlparse")


def get_refactoring_plan() -> Tuple[Project, ChangeSet]:
    """
    Load djongo module and return ChangeSet object renaming
    "sqlparse" to "djongo.sqlparse"

    Inspired from https://stackoverflow.com/a/59208439

    :return: Rope Project object and ChangeSet object representing
        changes to be made
    :rtype: tuple
    """

    proj = Project("djongo")
    sqlparse = proj.get_module("sqlparse").get_resource()
    change = Rename(proj, sqlparse).get_changes("djongo.sqlparse")
    return proj, change


if __name__ == "__main__":

    import sys

    LOG_FORMATTER = "%(asctime)s %(levelname)-8s [%(name)s] %(message)s"
    logging.basicConfig(level=logging.DEBUG, format=LOG_FORMATTER, stream=sys.stdout)

    LOGGER.info('Asking rope to prepare changeset to rename "sqlparse module" to "djongo.sqlparse"')
    PROJECT, CHANGESET = get_refactoring_plan()
    assert CHANGESET.changes, "Rope changeset is empty, you probably ran this script from the wrong directory"
    DIFF = CHANGESET.get_description()
    for line in DIFF.splitlines():
        LOGGER.debug(line)
    LOGGER.info("Changeset will no be applied to files")
    PROJECT.do(CHANGESET)
    LOGGER.info("Refactoring done")
